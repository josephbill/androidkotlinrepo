package com.emobilis.kotlinclass2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout

class TabLayoutActivity : AppCompatActivity() {

    //declaring my adapter and also my viewpager

    private lateinit var  mSectionsPagerAdapter:SectionsPagerAdapter

    /**
     * The {@link ViewPager} that will host the section contents.
     */


    private lateinit var mViewPager: ViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab_layout)


        //finding the toolbar view
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)


        //creating an instance of the Pager adapter in my oncreate
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        //finding my view pager and setting the adapter it has to use
        mViewPager = findViewById(R.id.container) as ViewPager
        mViewPager.setAdapter(mSectionsPagerAdapter)


        //finding my tab layout
        val tabLayout = findViewById(R.id.tabLayout) as TabLayout

        //connecting the tabLayout with the viewpager
        tabLayout.setupWithViewPager(mViewPager)


    }

    //creating the options menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.bottom_menu,menu)
        return true
    }

    //on click when an item is selected
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.bottom_frag_A){
            Toast.makeText(this,"Fragment A clicked",Toast.LENGTH_SHORT).show()
        } else if (id == R.id.bottom_frag_B){
            val intent: Intent = Intent(this@TabLayoutActivity,NavigationActivity::class.java)
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }


    class PlaceholderFragment : Fragment(){


        fun newInstance(sectionNumber:Int) : PlaceholderFragment{

            //tag for selected fragment from tab options
            val ARG_SECTION_NUMBER = "section_number"

            val fragment = PlaceholderFragment()

            //new instance of the Bundle class to use to hold our items or in this case the fragment in the placeholder view
            val args = Bundle()

            args.putInt(ARG_SECTION_NUMBER, sectionNumber)

            fragment.setArguments(args)

            return fragment
        }

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            //create A Ref to the layout inflated in this class
            val rootview = inflater.inflate(R.layout.fragment_a,container,false)
            val textView = rootview.findViewById(R.id.text) as TextView
            textView.text = "active fragment"
            return rootview
        }

    }


    //sections pager adapter
    class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm){

        override fun getItem(position: Int): Fragment {
            //create a ref to the Fragment class
            var fragment: Fragment? = null
            when(position){
                0 -> {
                    fragment = FragmentA()
                }
                1 -> {
                    fragment = FragmentB()
                }
            }
            return fragment!!
        }

        override fun getCount(): Int {
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence? {
            when(position){
                0 -> {
                    return "Fragment A"
                }
                1 -> {
                    return "Fragment B"
                }
            }
            return null
        }

    }
}
