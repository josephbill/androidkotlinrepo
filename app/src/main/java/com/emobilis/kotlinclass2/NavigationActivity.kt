package com.emobilis.kotlinclass2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView

class NavigationActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    //declare ref to the drawer layout
    //variable to hold ref to the drawer layout
    private lateinit var drawer: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //find toolbar
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        //find drawer layout
        drawer = findViewById(R.id.drawer_layout)

        //find navigation view
        val navigationView: NavigationView = findViewById(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)

        //new instance of the ActionBarToggle class so that we can get the hamburger icon
        val toggle: ActionBarDrawerToggle = ActionBarDrawerToggle(this,drawer, toolbar,
            R.string.navigation_drawer_open,R.string.navigation_drawer_close)
        //attach a drawer listner for tha toggle effect
        drawer.addDrawerListener(toggle)
        //sync state
        toggle.syncState()

        //we check the availability of our frame layout
        if (savedInstanceState == null){
            //if the state is null load a fragment to the frame layout in this class
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container,FragmentA()).commit()
            navigationView.setCheckedItem(R.id.nav_fragmentA)
        }

    }

    //swich between the links in the menu file
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.nav_fragmentA -> {
                supportFragmentManager.beginTransaction().replace(R.id.fragment_container,FragmentA()).commit()
            }
            R.id.nav_fragmentB -> {
                supportFragmentManager.beginTransaction().replace(R.id.fragment_container,FragmentB()).commit()
            }
            R.id.nav_bottom -> {
                val intent: Intent =  Intent(this@NavigationActivity,BottomNavigationActivity::class.java)
                startActivity(intent)
            }
            R.id.tabLay -> {
                val intent: Intent =  Intent(this@NavigationActivity,TabLayoutActivity::class.java)
                startActivity(intent)
            }
            R.id.toast -> {
                Toast.makeText(this,"Toast linked clicked",Toast.LENGTH_SHORT).show()
            }
        }
        return true
    }


    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
}
