package com.emobilis.kotlinclass2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout

class BottomNavigationActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_navigation)

        //find view by id
        val bottomNav = findViewById<BottomNavigationView>(R.id.bottom_nav)

        //attach widget with Listener
        bottomNav.setOnNavigationItemSelectedListener(this)

        //checking if fragment is empty and loading a view if so
        if (savedInstanceState == null){
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container,FragmentA()).commit()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        //creating a fragment class reference
        val selectedFragment: Fragment? = null

        when(item.itemId){
            R.id.bottom_frag_A -> {
                supportFragmentManager.beginTransaction().replace(R.id.fragment_container,FragmentA()).commit()

            }
            R.id.bottom_frag_B -> {
                supportFragmentManager.beginTransaction().replace(R.id.fragment_container,FragmentB()).commit()

            }
            R.id.bottom_nav -> {
                val intent: Intent =  Intent(this@BottomNavigationActivity,NavigationActivity::class.java)
                startActivity(intent)
            }
            R.id.bottom_tab -> {
                val intent: Intent =  Intent(this@BottomNavigationActivity,TabLayoutActivity::class.java)
                startActivity(intent)
            }
        }
        return true
    }
}
